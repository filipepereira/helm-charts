{{/*
Expand the name of the chart.
*/}}
{{- define "selenosis.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "selenosis.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified seleniferous name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "selenosis.seleniferous.fullname" -}}
{{- if .Values.seleniferous.fullnameOverride }}
{{- .Values.seleniferous.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- printf "%s-%s" .Release.Name .Values.seleniferous.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- printf "%s-%s-%s" .Release.Name $name .Values.seleniferous.name | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified selenoid ui name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "selenosis.selenoidUi.fullname" -}}
{{- if .Values.selenoidUi.fullnameOverride }}
{{- .Values.selenoidUi.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- printf "%s-%s" .Release.Name .Values.selenoidUi.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- printf "%s-%s-%s" .Release.Name $name .Values.selenoidUi.name | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "selenosis.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "selenosis.common.labels" -}}
helm.sh/chart: {{ include "selenosis.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "selenosis.labels" -}}
{{ include "selenosis.common.labels" . }}
{{ include "selenosis.selectorLabels" . }}
{{- end }}

{{- define "selenosis.selenoidUi.labels" -}}
{{ include "selenosis.common.labels" . }}
{{ include "selenosis.selenoidUi.selectorLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "selenosis.common.selectorLabels" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "selenosis.selectorLabels" -}}
app.kubernetes.io/name: {{ include "selenosis.name" . }}
{{ include "selenosis.common.selectorLabels" . }}
{{- end }}

{{- define "selenosis.selenoidUi.selectorLabels" -}}
app.kubernetes.io/name: {{ .Values.selenoidUi.name }}
{{ include "selenosis.common.selectorLabels" . }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "selenosis.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "selenosis.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "selenosis.selenoidUi.authorizationHeaders" -}}
httpHeaders:
  - name: Authorization
    value: Basic {{ printf "%s:%s" .Values.selenoidUi.htpasswdFile.user .Values.selenoidUi.htpasswdFile.password | b64enc }}
{{- end }}
